import { BrowserRouter, Route, Routes } from 'react-router-dom';
import ContactUs from '../pages/ContactUs';
import Home from '../pages/Home';
import '../../common/scss/main.scss';
import './App.scss';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/contact-us' element={<ContactUs />} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;
