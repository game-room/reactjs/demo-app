import './Home.scss';
//BEM
// BLOCK__ELEMENT--modifier

function Home() {
    return (
        <section className="home">
            <header className="home__header">
                <h1 className="home__header-title">
                    Home
                </h1>
            </header>
        </section>
    );
}

export default Home;
