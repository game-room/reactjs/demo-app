import './ContactUs.scss';

//BEM
// BLOCK__ELEMENT--modifier

function ContactUs() {
    return (
        <section className="contact-us">
            <header className="contact-us__header">
                <h1 className="contact-us__header-title">
                    ContactUs
                </h1>
            </header>
        </section>
    );
}

export default ContactUs;
